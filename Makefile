# Run server

main:
	. venv/bin/activate; \
	python source/main.py data/thesaurus.rdf; \

# Generate necessary data

data/thesaurus.dict:
	mkdir -p data; \
	. venv/bin/activate; \
	python source/getthesaurus.py \
		--outputfile=$@; \

data/thesaurus.rdf: data/thesaurus.dict
	mkdir -p data; \
	. venv/bin/activate; \
	python source/dict2rdf.py \
		--dict-file=data/thesaurus.dict \
		--outputfile=$@; \

gendata: data/thesaurus.rdf

# Clean

clean:
	find . -name "*~" -delete

# Install dependencies

venv:
	tar -xf dependencies/virtualenv-13.1.0.tar.gz; \
	./virtualenv-13.1.0/virtualenv.py venv; \
	rm -r virtualenv-13.1.0/; \
	. venv/bin/activate; \
	pip install -r venv_packages.txt; \

venvupdate:
	rm -r venv; \
	make install_dependencies; \

venvinstall:
	if [ ! -z "$(pkg)" ]; then \
		. venv/bin/activate; \
		pip install --upgrade $(pkg) && \
			pip freeze > venv_packages.txt; \
	fi; \

install_dependencies: venv
