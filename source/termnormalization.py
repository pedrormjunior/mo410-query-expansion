#!usr/bin/python
#-*- coding: utf-8 -*-

import unicodedata

def normalize(term):
    assert isinstance(term, unicode)

    normalized = ''.join(
        c for c in unicodedata.normalize('NFD', term)
        if unicodedata.category(c) != 'Mn'
    )

    normalized = normalized.lower()

    return normalized

def normalizeRDF(term):
    assert isinstance(term, unicode)

    normalized = normalize(term)

    parenthesis_loc = normalized.find('(')
    if parenthesis_loc > -1:
        normalized = normalized[:parenthesis_loc].strip()

    return normalized
