#!usr/bin/python
#-*- coding: utf-8 -*-

import sys
import os
import traceback
import shlex
from pprint import pprint
import json

import re
import argparse

import requests
from flask import Flask, request, redirect, url_for, send_from_directory, render_template, Response
from werkzeug import secure_filename

from search_form import SearchForm, UseButton

from rdf_query_engine import *

from termnormalization import normalize

## --- Parsing command line arguments -- ##

parser = argparse.ArgumentParser()
parser.add_argument("tesfpath",
                    help="Path to Tesaurus RDF file",
                    type=str)
args = parser.parse_args()

## ------------------------------------- ##


RDFE = RdfQueryEngine(args.tesfpath)

app = Flask(__name__)

sniic_url = "http://sniic.cultura.gov.br/index.php/usuarioInternetAction/consultaInformacoesList"


def create_jquery_string(terms_list):

    namestr =    " OR(ilike(%{0:s} %), ilike(% {0:s} %), ilike(% {0:s}%)".format(terms_list[0])
    longdestr =  " OR(ilike(%{0:s} %), ilike(% {0:s} %), ilike(% {0:s}%)".format(terms_list[0])
    shortdestr = " OR(ilike(%{0:s} %), ilike(% {0:s} %), ilike(% {0:s}%)".format(terms_list[0])

    for term in terms_list[1:]:
        namestr +=    ", ilike(%{0:s} %), ilike(% {0:s} %), ilike(% {0:s}%)".format(term)
        longdestr +=  ", ilike(%{0:s} %), ilike(% {0:s} %), ilike(% {0:s}%)".format(term)
        shortdestr += ", ilike(%{0:s} %), ilike(% {0:s} %), ilike(% {0:s}%)".format(term)

    namestr += ")"
    longdestr += ")"
    shortdestr += ")"

    return namestr, longdestr, shortdestr

def parse_keywords(keyword_string):

    aux = normalize(keyword_string)
    aux = shlex.split(aux)

    kwlist = [y for x in aux for y in re.split(',|;|; |, |\+|\+ | \+ ', x) if y != '']

    return kwlist



@app.route('/searchsniic', methods=['POST'])
def query_sniic():

    try:
        fq = request.form.getlist('term_synonym') + request.form.getlist('term_generic')
        fq += request.form.getlist('term_narrower') + request.form.getlist('term_related')

        sfield = request.form.getlist("sniic_field")[0]
        sfield = "{0:s}".format(sfield)
        #print "sniic field: ", sfield

        print "full query is:"
        #print fq
        map(pprint, fq)

        if not fq:
            return redirect(url_for('search_interface'))

        else:
            ns, ls, ss = create_jquery_string(fq)
            #print ns
            #print ls
            #print ss
            return render_template('sniic_results.html', terms=json.dumps(fq), names=json.dumps(ns), longdes=json.dumps(ls),
                                   shortdes=json.dumps(ss), sniic_field=json.dumps(sfield))

    except Exception as e:
        print "Problem with query!"
        e_type, e_val, e_tb = sys.exc_info()
        traceback.print_exception(e_type, e_val, e_tb)

@app.route('/qexpansion', methods=['GET', 'POST'])
def search_interface():
    sform = SearchForm(request.form)

    if request.method == 'POST' and sform.validate():
        try:
            kw_string = sform.keywords.data
            qe = sform.query_exp.data
            depth = sform.depth.data

            kw_list = parse_keywords(kw_string)

            norm_kw_list = map(normalize, [s.decode('utf-8') for s in kw_list])

            print "Keywords: "
            for kw in kw_list:
                print "   > ", kw

            print "Expansion? ", qe
            print "Related Depth: ", depth

            expanded = dict(synonyms=[],
                            narrower=[],
                            generic=[],
                            related=[])

            if qe:

                expanded['synonyms'].extend(RDFE.synonymous(norm_kw_list))
                expanded['narrower'].extend(RDFE.narrower(norm_kw_list))
                expanded['generic'].extend(RDFE.broader(norm_kw_list))
                expanded['related'].extend(RDFE.related(norm_kw_list, depth=depth))

            expanded['original'] = norm_kw_list

            syn = [t[0] for t in expanded['synonyms']]
            for t in expanded['original']:
                if t not in syn:
                    expanded['synonyms'].insert(0, (t, "https://www.google.com.br/#q={0:s}".format(t)))

            aux_syn = set(expanded['synonyms'])
            aux_rel = set(expanded['related'])

            expanded['related'] = list(aux_rel - aux_syn)

            return render_template('qexpansion_m.html', form=sform, expansion=expanded, oterm='+'.join(norm_kw_list))

        except Exception as e:
            print "Problem with query!"
            e_type, e_val, e_tb = sys.exc_info()
            traceback.print_exception(e_type, e_val, e_tb)

    return render_template('qexpansion_m.html', form=sform)


# Starts the Flask APP
if __name__ == '__main__':
    app.run(host='0.0.0.0', threaded=True)