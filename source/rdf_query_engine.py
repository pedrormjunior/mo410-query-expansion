#!usr/bin/python
#-*- coding: utf-8 -*-

import rdflib

class RdfQueryEngine(object):
    def __init__(self, rdffile):
        self._RdfGraph = rdflib.Graph()
        self._RdfGraph.parse(rdffile)

    def get_graph_lenght(self):
        return len(self._RdfGraph)

    def narrower(self, terms):
        return self._search(terms, 'narrower')

    def broader(self, terms):
        return self._search(terms, 'broader')

    def related(self, terms, depth=1):
        return self._search(terms, 'related', depth=depth)

    def synonymous(self, terms):
        return self._search(terms, 'synonymous')

    def _searchterm(self, term, cls):
        assert isinstance(term, unicode)
        assert isinstance(cls, str)
        assert cls in ['narrower', 'broader', 'related', 'synonymous', ]

        literal_term = rdflib.Literal(term)

        query_string = \
            ("PREFIX skos:<http://www.w3.org/2004/02/skos/core#> SELECT ?finalterm ?b WHERE { " +
             "{{?a skos:prefLabel ?searchTerm . ?a skos:{cls} ?b . ?b skos:prefLabel ?finalterm}} UNION "
             "{{?a skos:altLabel ?searchTerm . ?a skos:{cls} ?b . ?b skos:prefLabel ?finalterm}} UNION "
             "{{?a skos:prefLabel ?searchTerm . ?a skos:{cls} ?b . ?b skos:altLabel ?finalterm}} UNION "
             "{{?a skos:altLabel ?searchTerm . ?a skos:{cls} ?b . ?b skos:altLabel ?finalterm}} ".format(cls=cls) +
             "}") \
             if cls != 'synonymous' else \
            ("PREFIX skos:<http://www.w3.org/2004/02/skos/core#> SELECT ?finalterm ?a WHERE { " +
             "{?a skos:prefLabel ?searchTerm . ?a skos:prefLabel ?finalterm} UNION "
             "{?a skos:prefLabel ?searchTerm . ?a skos:altLabel ?finalterm} UNION "
             "{?a skos:altLabel ?searchTerm . ?a skos:prefLabel ?finalterm} UNION "
             "{?a skos:altLabel ?searchTerm . ?a skos:altLabel ?finalterm} " +
             "}")

        query_results = self._RdfGraph.query(
            query_string, initBindings={'searchTerm': literal_term, }
        )

        f = lambda termtuple: tuple(map(lambda x: x.toPython(), termtuple))
        results = map(f, query_results)

        return results

    def _search(self, terms, cls, depth=1):
        def prepareterms(terms):
            assert isinstance(terms, (list, str, unicode))
            assert not isinstance(terms, list) or \
                all(isinstance(term, (str, unicode)) for term in terms)
            terms = terms if isinstance(terms, list) else [terms]
            f = lambda term: term.decode('utf-8') if isinstance(term, str) else term
            return map(f, terms)

        terms = prepareterms(terms)
        assert isinstance(cls, str)
        assert isinstance(depth, int)
        assert depth == 1 or (cls == 'related' and depth > 0)

        expandedterms = [self._searchterm(term, cls) for term in terms]
        while [] in expandedterms: expandedterms.remove([])
        expandedterms = reduce(lambda terms1, terms2: terms1 + terms2, expandedterms) \
                        if len(expandedterms) > 0 else expandedterms

        if depth > 1:
            expandedterms = list(set(
                expandedterms +
                self._search(map(lambda (term, link): term, expandedterms), cls, depth=depth-1)
            ))

        return expandedterms
