#!usr/bin/python
#-*- coding: utf-8 -*-

import urllib2
import itertools as it

import os
import sys

import mygetopt

optlist = mygetopt.mygetopt(sys.argv[1:], [
    'outputfile=',
])

for o, a in optlist:
    if o == '--outputfile':
        outputfilename = os.path.expanduser(a)
        assert outputfilename.endswith('.dict')
    else:
        raise ValueError

def gettermid(line):
    assert isinstance(line, unicode)
    return line.split('href="')[1].split('"')[0]

thesaurus = {}

for i in range(97, 122+1):
    char = chr(i)
    link = 'http://www.cnfcp.gov.br/tesauro/index_{}.htm'.format(char)

    response = urllib2.urlopen(link)
    html = response.read()
    html = html.decode('latin1')

    lines = html.split('\n')
    lines = filter(lambda line: line.find('a class="lista-termo"') != -1, lines)
    ids = map(gettermid, lines)

    for id_term in ids:
        linkmodel = 'http://www.cnfcp.gov.br/tesauro/{}'
        link = linkmodel.format(id_term)
        thesaurus[link] = {}

        response = urllib2.urlopen(link)
        html = response.read()
        html = html.decode('latin1')

        lines = html.split('\n')
        lines = list(it.dropwhile(lambda line: line.find('id="principal"') == -1, lines))

        term = lines[1].split('>')[1].split('<')[0]
        thesaurus[link]['term'] = term
        print u'Downloading information about the term "{}"'.format(term)

        def filtercategory(categoryname, singleline=False):
            categorylines = list(it.dropwhile(lambda line: line.find(categoryname) == -1, lines))
            if singleline:
                categorylines = [categorylines[0]] if len(categorylines) > 0 else categorylines
            else:
                categorylines = list(it.takewhile(lambda line: line.find('</ul>') == -1, categorylines))
            categorylines = filter(lambda line: line.find('a class="termo"') != -1, categorylines)
            categorylines = map(gettermid, categorylines)
            categorylines = map(lambda auxid: linkmodel.format(auxid), categorylines)
            return categorylines

        used_by = filtercategory('<h3>Usado por')
        use = filtercategory('<h3>Use', singleline=True)
        generic = filtercategory('<h3>Termo Gen&eacute;rico')
        associated = filtercategory('<h3>Termos Associados')
        specific = filtercategory('<h3>Termos Espec&iacute;ficos')

        thesaurus[link]['used_by'] = used_by
        thesaurus[link]['use'] = use
        thesaurus[link]['generic'] = generic
        thesaurus[link]['associated'] = associated
        thesaurus[link]['specific'] = specific

fd = open(outputfilename, 'w')
print >> fd, thesaurus
fd.close()
