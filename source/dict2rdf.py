#!usr/bin/python
#-*- coding: utf-8 -*-

import sys
import os

import mygetopt

from rdflib.namespace import SKOS
import rdflib

import termnormalization as tn

optlist = mygetopt.mygetopt(sys.argv[1:], [
    'dict-file=',
    'outputfile=',
])

for o, a in optlist:
    if o == '--dict-file':
        dictfilename = os.path.expanduser(a)
        assert dictfilename.endswith('.dict')
        assert os.path.exists(dictfilename)
    elif o == '--outputfile':
        rdffilename = os.path.expanduser(a)
        assert rdffilename.endswith('.rdf')
    else:
        raise ValueError

fd = open(dictfilename)
thesaurus = fd.readlines()
fd.close()
assert len(thesaurus) == 1
thesaurus = eval(thesaurus[0])

graph = rdflib.Graph()

for key in thesaurus.keys():
    assert len(thesaurus[key]['use']) <= 1

    if len(thesaurus[key]['use']) > 0:
        """This term is going to be referred to by another term as a skos:altLabel."""
        continue

    related_terms = ['associated', 'specific', 'generic']
    literal_terms = ['used_by']
    mapskos = {
        'associated': SKOS.related,
        'specific': SKOS.narrower,
        'generic': SKOS.broader,
        'used_by': SKOS.altLabel,
    }

    graph.add([
        rdflib.URIRef(key),
        SKOS.prefLabel,
        rdflib.Literal(tn.normalizeRDF(thesaurus[key]['term'])),
    ])

    for dict_term in literal_terms:
        for cod in thesaurus[key][dict_term]:
            graph.add([
                rdflib.URIRef(key),
                mapskos[dict_term],
                rdflib.Literal(tn.normalizeRDF(thesaurus[cod]['term'])),
            ])

    for dict_term in related_terms:
        for cod in thesaurus[key][dict_term]:
            graph.add([
                rdflib.URIRef(key),
                mapskos[dict_term],
                rdflib.URIRef(cod),
            ])

fd = open(rdffilename, 'w')
print >> fd, graph.serialize()
fd.close()
