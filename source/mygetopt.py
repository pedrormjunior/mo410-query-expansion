import getopt

def mygetopt(argv, argopts):
    optlist, args = getopt.getopt(argv, '', argopts)

    opts = map(lambda (o, a): o[2:], optlist)
    optsdef = map(lambda o: o[:-1], argopts)
    assert all([o in opts for o in optsdef]), 'all arguments must be provided'

    return optlist
