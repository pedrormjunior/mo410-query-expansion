#!usr/bin/python
#-*- coding: utf-8 -*-

from wtforms import Form, BooleanField, TextField, validators, IntegerField


class SearchForm(Form):

    keywords = TextField('Keywords', [validators.Required()])
    query_exp = BooleanField('Expand query?')
    depth = IntegerField('Search Depth', validators=[validators.NumberRange(min=1, max=4)])

class UseButton(Form):

    use = BooleanField('Use?')


